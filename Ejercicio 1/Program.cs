﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
	class Program
	{
		//  Programacion I
		//	Nicolas Aybar Critto
		//	Comision 2
		public static void Menu()
		{
			Console.WriteLine("1- Mostrar Matriz");
			Console.WriteLine("2- Sumar Matriz");
			Console.WriteLine("3- Promedio Matriz");
			Console.WriteLine("4- Esta el target en matriz");
			Console.WriteLine("5- Veces que se repite el target en matriz");
			Console.WriteLine("6- Mayor en matriz");
			Console.WriteLine("7- Menor en matriz");
			Console.WriteLine("8- Destacar celda pares");
			Console.WriteLine("9- Destacar celda impares");
			Console.WriteLine("10- Destacar multiplo");
			Console.WriteLine("11- Destacar mayores");
			Console.WriteLine("12- Destacar entre");
			Console.WriteLine("13- Sumar fila matriz");
			Console.WriteLine("14- Sumar fila matriz");
			Console.WriteLine("15- Promedio fila");
			Console.WriteLine("16- Promedio columna");
			Console.WriteLine("17- Fila sumatoria mayor");
			Console.WriteLine("18- Columna sumatoria mayor");
			Console.WriteLine("0- Salir");
			Console.Write("\nIngrese una opcion: ");
		}

		public static void CargaMatriz(int[,] mat, int minimo, int maximo)
		{
			Random generador = new Random();
			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{
					mat[f, c] = generador.Next(minimo + 1, maximo);
				}
			}
		}

		public static void MostrarMatriz(int[,] mat, int minimo, int maximo)
		{
			Console.WriteLine($"---Se ha generado la siguiente matriz ---");
			Console.WriteLine();
			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{
					Console.Write($"{mat[f, c]} ");
				}
				Console.WriteLine();
			}
		}

		public static int SumarMatriz(int[,] mat)
		{
			int _res = 0;

			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{
					_res += mat[f, c];
				}
			}
			return _res;
		}

		public static int PromedioMatriz(int[,] mat)
		{
			int _res = 0;
			int _contador = 0;

			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{
					_contador++;
					_res += mat[f, c];

				}
			}
			_res = _res / _contador;
			return _res;
		}

		public static bool EstaEnMatriz(int[,] mat, int target)
		{
			bool pertenece = false;

			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{
					if (target == mat[f, c])
					{
						pertenece = true;
						break;
					}
				}
			}

			if (pertenece == true)
			{
				Console.WriteLine($"El numero {target} pertenece a la matriz!");
			}
			else
			{
				Console.WriteLine($"El numero {target} no pertenece a la matriz!");
			}
			return pertenece;
		}

		public static int VecesEnMatriz(int[,] mat, int target)
		{
			int _contador = 0;

			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{
					if (target == mat[f, c])
					{
						_contador++;
					}
				}
			}
			return _contador;
		}

		public static int MayorEnMatriz(int[,] mat)
		{
			int mayor = int.MinValue;

			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{
					if (mayor < mat[f, c])
					{
						mayor = mat[f, c];
					}
				}
			}
			return mayor;
		}

		public static int MenorEnMatriz(int[,] mat)
		{
			int menor = int.MaxValue;

			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{
					if (menor > mat[f, c])
					{
						menor = mat[f, c];
					}
				}
			}
			return menor;
		}
		public static void CeldaPares(int[,] mat)
		{
			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{

					if ((mat[f, c]) % 2 == 0)
					{
						Console.ForegroundColor = ConsoleColor.Green;
					}
					else
					{
						Console.ResetColor();
					}
					Console.Write($"{mat[f, c]} ");
				}
				Console.WriteLine();
			}
		}

		public static void CeldaImares(int[,] mat)
		{
			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{

					if ((mat[f, c]) % 2 == 1)
					{
						Console.ForegroundColor = ConsoleColor.Red;
					}
					else
					{
						Console.ResetColor();
					}
					Console.Write($"{mat[f, c]} ");
				}
				Console.WriteLine();
			}
		}

		public static void DestacarMultiplo(int[,] mat, int multiplo)
		{
			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{

					if ((mat[f, c]) % multiplo == 0)
					{

						Console.ForegroundColor = ConsoleColor.Green;

					}
					else
					{
						Console.ResetColor();
					}
					Console.Write($"{mat[f, c]} ");
				}
				Console.WriteLine();
			}
		}

		public static void DestacarMayor(int[,] mat, int mayor)
		{
			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{

					if ((mat[f, c]) > mayor)
					{
						Console.ForegroundColor = ConsoleColor.Green;
					}
					else
					{
						Console.ResetColor();
					}
					Console.Write($"{mat[f, c]} ");
				}
				Console.WriteLine();
			}
		}

		public static void DestacarEntre(int[,] mat, int numuno, int numdos)
		{
			for (int f = 0; f < mat.GetLength(0); f++)
			{
				for (int c = 0; c < mat.GetLength(1); c++)
				{

					if ((numuno < mat[f, c]) && (mat[f, c] < numdos))
					{
						Console.ForegroundColor = ConsoleColor.Green;
					}
					else
					{
						Console.ResetColor();
					}
					Console.Write($"{mat[f, c]} ");
				}
				Console.WriteLine();
			}
		}

		public static int SumarFila(int[,] mat, int fil)
		{
			int res = 0;

			for (int i = 0; i < mat.GetLength(1); i++)
			{
				res += mat[fil - 1, i];
			}
			return res;
		}

		public static int SumarColumna(int[,] mat, int col)
		{
			int res = 0;

			for (int i = 0; i < mat.GetLength(0); i++)
			{
				res += mat[i, col - 1];
			}
			return res;
		}

		public static int PromediarFila(int[,] mat, int fil)
		{
			int suma = 0;
			int contador = 0;
			for (int i = 0; i < mat.GetLength(1); i++)
			{
				contador++;
				suma += mat[fil - 1, i];
			}
			suma = suma / contador;
			return suma;
		}

		public static int PromediarColumna(int[,] mat, int col)
		{
			int suma = 0;
			int contador = 0;
			for (int i = 0; i < mat.GetLength(0); i++)
			{
				contador++;
				suma += mat[i, col - 1];
			}
			suma = suma / contador;
			return suma;
		}
		public static int FilaMayor(int[,] mat)
		{

			int _mayor = int.MinValue;
			int _posicion = 0;
			int _sumaFila = 0;

			for (int f = 0; f < mat.GetLength(0); f++) // Corre las filas
			{
				_sumaFila = 0;
				for (int c = 0; c < mat.GetLength(1); c++)
				{
					_sumaFila += mat[f, c];

					if (_sumaFila > _mayor)
					{
						_mayor = _sumaFila;
						_posicion = f;
					}
				}

			}

			return _posicion;
		}

		public static int ColumnaMayor(int[,] mat)
		{

			int _mayor = int.MinValue;
			int _posicion = 0;
			int _sumaColumna = 0;

			for (int c = 0; c < mat.GetLength(1); c++) // Corre las filas
			{
				_sumaColumna = 0;
				for (int f = 0; f < mat.GetLength(0); f++)
				{
					_sumaColumna += mat[f, c];

					if (_sumaColumna > _mayor)
					{
						_mayor = _sumaColumna;
						_posicion = c;
					}
				}

			}

			return _posicion;
		}

		static void Main(string[] args)
		{
			//Declaracion de variables de entrada
			const int FILAS = 3;
			const int COLUMNAS = 3;
			int[,] mat = new int[FILAS, COLUMNAS];
			int _minimo = 0;
			int _maximo = 0;
			int _opcion = 0;

			bool _verificarDato = false;
			bool _finalizarPrograma = false;
			bool _entradaDatos = false;

			//Proceso de variables
			int _suma = 0;
			double _promedio = 0.0;
			int _vecesEnMatriz = 0;
			int _target = 0;
			int _multiplo = 0;
			int _mayor = 0;
			int _numeroUno = 0;
			int _numeroDos = 0;
			int _filas = 0;
			int _sumaFilas = 0;
			int _columna = 0;
			int _sumaColumnas = 0;
			int _promedioFilas = 0;
			int _promedioColumnas = 0;
			int _indice = 0;


			while (_entradaDatos == false)
			{
				//Captura de datos
				_verificarDato = false;
				while (_verificarDato == false)
				{
					Console.Write("Ingrese el numero minimo para la matriz: ");
					if (int.TryParse(Console.ReadLine(), out _minimo) && _minimo > 0)
					{
						_verificarDato = true;
					}
				}
				_verificarDato = false;
				while (_verificarDato == false)
				{

					Console.Write("Ingrese el numero maximo para la matriz: ");
					if (int.TryParse(Console.ReadLine(), out _maximo) && _maximo > _minimo)
					{
						_verificarDato = true;
						_entradaDatos = true;
					}
				}
			}

			//Operaciones
			CargaMatriz(mat, _minimo, _maximo);
			Console.Clear();

			//Menu de la matriz
			while (_finalizarPrograma == false)
			{
				_verificarDato = false;
				while (_verificarDato == false)
				{
					Console.Clear();
					Menu();
					if (int.TryParse(Console.ReadLine(), out _opcion))
					{
						Console.WriteLine();
						switch (_opcion)
						{
							case 1:
								MostrarMatriz(mat, _minimo, _maximo);
								break;
							case 2:
								_suma = SumarMatriz(mat);
								Console.WriteLine($"La suma de los elementos de la matriz es igual a {_suma}");
								break;
							case 3:
								_promedio = PromedioMatriz(mat);
								Console.WriteLine($"El promedio de la matriz es igual a {_promedio}");
								break;
							case 4:
								Console.Write("Ingrese un numero para verificar si esta en la matriz: ");
								if (int.TryParse(Console.ReadLine(), out _target))
								{
									EstaEnMatriz(mat, _target);
								}
								else
								{
									Console.WriteLine("Ocurrio un error. Por favor ingrese un numero");
								}
								break;
							case 5:
								Console.Write("Ingrese un numero para verificar cuantas veces esta en la matriz: ");
								if (int.TryParse(Console.ReadLine(), out _target))
								{
									_vecesEnMatriz = VecesEnMatriz(mat, _target);
								}
								else
								{
									Console.WriteLine("Ocurrio un error. Por favor ingrese un numero");
								}
								Console.WriteLine($"El numero {_target} se repite {_vecesEnMatriz} vece(s) en la matriz");
								break;
							case 6:
								_maximo = MayorEnMatriz(mat);
								Console.WriteLine($"El mayor numero de la matriz es {_maximo}");
								break;
							case 7:
								_minimo = MenorEnMatriz(mat);
								Console.WriteLine($"El minimo numero de la matriz es {_minimo}");
								break;
							case 8:
								CeldaPares(mat);
								break;
							case 9:
								CeldaImares(mat);
								break;
							case 10:
								Console.Write("Ingrese el numero al cual desee destacar los multiplos: ");
								if (int.TryParse(Console.ReadLine(), out _multiplo) && _multiplo != 0)
									DestacarMultiplo(mat, _multiplo);
								else
								{
									Console.WriteLine("No se puede realizar la operacion, ya que no existe la division en cero");
								}
								break;
							case 11:
								Console.Write("Ingrese el numero al cual desee destacar sus mayores: ");
								if (int.TryParse(Console.ReadLine(), out _mayor))
								{
									DestacarMayor(mat, _mayor);
								}
								else
								{
									Console.WriteLine("Ocurrio un error. Por favor ingrese un numero para destacar sus mayores");
								}
								break;
							case 12:
								Console.Write("Ingrese el primer numero: ");
								if (int.TryParse(Console.ReadLine(), out _numeroUno))
									Console.Write("Ingrese el segundo numero (mayor al primer numero ingresado): ");
								if (int.TryParse(Console.ReadLine(), out _numeroDos) && _numeroDos > _numeroUno)
								{
									DestacarEntre(mat, _numeroUno, _numeroDos);
								}
								else
								{
									Console.WriteLine("Ocurrio un error. Ingrese un numero mayor al primer numero");
								}

								break;
							case 13:
								Console.Write("Ingrese la fila que desee sumar: ");
								if (int.TryParse(Console.ReadLine(), out _filas) && _filas >= 1 && _filas <= 3)
									_sumaFilas = SumarFila(mat, _filas);
								else
								{
									Console.WriteLine("Ocurrio un error. Las filas deben ser entre 1 y 3");
								}
								Console.WriteLine($"La suma de la fila {_filas} es igual a {_sumaFilas}");

								break;
							case 14:
								Console.Write("Ingrese la columna que desee sumar: ");
								if (int.TryParse(Console.ReadLine(), out _columna) && _columna >= 1 && _columna <= 3)
									_sumaColumnas = SumarColumna(mat, _columna);
								else
								{
									Console.WriteLine("Ocurrio un error. Las columnas deben ser entre 1 y 3");
								}
								Console.WriteLine($"La suma de la fila {_columna} es igual a {_sumaColumnas}");
								break;
							case 15:
								Console.Write("Ingrese la fila que desee promediar: ");
								if (int.TryParse(Console.ReadLine(), out _filas))
									_promedioFilas = PromediarFila(mat, _filas);
								Console.WriteLine($"El promedio de la fila {_filas} es igual a {_promedioFilas}");
								break;
							case 16:
								Console.Write("Ingrese la columna que desee promediar: ");
								if (int.TryParse(Console.ReadLine(), out _columna))
									_promedioColumnas = PromediarFila(mat, _columna);
								Console.WriteLine($"El promedio de la columna {_columna} es igual a {_promedioColumnas}");
								break;
							case 17:
								_indice = FilaMayor(mat);
								Console.WriteLine($"La fila mas grande es la indice {_indice}");
								break;
							case 18:
								_indice = ColumnaMayor(mat);
								Console.WriteLine($"La columna mas grande es la indice {_indice}");
								break;
							case 0:
								_verificarDato = true;
								_finalizarPrograma = true;
								break;
						}
						Console.ResetColor();
						if (_opcion != 0)
						{
							Console.WriteLine("\nPresione cualquier tecla para seleccionar otra opcion...");
							Console.ReadKey();
						}
						else
						{
							Console.WriteLine("Presione cualquier tecla para finalizar...");
						}
						_verificarDato = true;
					}
				}
			}
			Console.ReadKey();
		}
	}
}